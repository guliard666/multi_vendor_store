require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
CONFIG = YAML.load(File.read(File.expand_path('../application.yml', __FILE__))).fetch(Rails.env, {})

module MultiVendorStore
  class Application < Rails::Application

    config.to_prepare do
      # Load application's model / class decorators
      Dir.glob(File.join(File.dirname(__FILE__), "../app/**/*_decorator*.rb")) do |c|
        Rails.configuration.cache_classes ? require(c) : load(c)
      end

      # Load application's view overrides
      Dir.glob(File.join(File.dirname(__FILE__), "../app/overrides/*.rb")) do |c|
        Rails.configuration.cache_classes ? require(c) : load(c)
      end
    end
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.active_job.queue_adapter = :delayed_job
    config.active_record.belongs_to_required_by_default = false

    ENV['PGHOST'] = CONFIG['database']['host']
		ENV['PGPORT'] = CONFIG['database']['port']
		ENV['PGUSER'] = CONFIG['database']['user']
		ENV['PGPASSWORD'] = CONFIG['database']['password']
    ENV['PUMA_WORKERS'] = CONFIG['rails']['application']['puma']['workers']
    ENV['PUMA_THREADS_MIN'] = CONFIG['rails']['application']['puma']['threads_min']
    ENV['PUMA_THREADS_MAX'] = CONFIG['rails']['application']['puma']['threads_max']
    ENV['PORT'] = CONFIG['rails']['application']['port']
    ENV['SECRET_KEY_BASE'] = CONFIG['secret_key_base']
  end
end
